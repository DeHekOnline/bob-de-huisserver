const http = require('http');
const player = require('play-sound')(opts={});
const fs = require('fs');
const ora = require('ora');

let header = `
#                                                    
#   _____     _     _                  _____     _   
#  |  |  |_ _|_|___| |_ ___ ___ ___   | __  |___| |_ 
#  |     | | | |_ -| . | .'| .'|_ -|  | __ -| . | . |
#  |__|__|___|_|___|___|__,|__,|___|  |_____|___|___|
#  
#   Created by Frank de Hek                                                                             
`;

console.clear();
console.log('\x1b[34m' + header + '\x1b[0m');
console.log('Currently listning on port 8080\n');
console.log('\x1b[33m');
const spinner = ora('Op huur aan het wachten ...').start();

http.createServer((req, res)=>{
    
    if(req.url !== '/favicon.ico') {
        spinner.succeed('Laat maar komme: ' + req.url);
        //console.log('\x1b[33m' + 'Aha! een request op: ' + req.url);
        var  url = req.url.replace(/^\/+/, '');
        spinner.start('Geef \'s antwoord');
        fs.access('sound/' + url + '.mp3', (err) => {
            if (err) {
                spinner.fail('Optyfen, gaauww!!');
                res.writeHead(404, {'Content-type': 'text/html'});
                res.end("Probeer je ons te hackeren ofzo?! Ga weg gek!1!1!!!");
                spinner.start('Op huur aan het wachten ... :)');
            } else {
                player.play('sound/' + url + '.mp3', (err)=> {
                    if (err) {
                    spinner.fail('Als je nou opgepast had ...');
                    throw (err);
                    } else {
                    spinner.succeed('U ken doorgaan ...');
                    spinner.start('Op huur aan het wachten ...');
                    }
                });
                res.writeHead(200, {'Content-type': 'text/html'}); 
                res.end("<html><head><title>Bob de Huisserver</title></head><h1>U ken doorgaan.</h1><html>");
            }
        });

    }

}).listen(8080);
